﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Elysium
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class MainMenu : Page
    {
        Admin adminPage = new Admin();
        public MainMenu()
        {
            InitializeComponent();
        }
        private void btnUser_MouseEnter(object sender, MouseEventArgs e)
        {
            lbl_buttonText.Content = "User";
        }

        private void btnUser_MouseLeave(object sender, MouseEventArgs e)
        {
            lbl_buttonText.Content = "";
        }

        private void btnAdmin_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(adminPage);
        }

        private void btnAdmin_MouseEnter(object sender, MouseEventArgs e)
        {
            lbl_buttonText.Content = "Administrator";
        }

        private void btnAdmin_MouseLeave(object sender, MouseEventArgs e)
        {
            lbl_buttonText.Content = "";
        }

        private void btnUser_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new User());
        }

       
    }
}
