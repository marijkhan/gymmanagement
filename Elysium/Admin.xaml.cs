﻿using Elysium.model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Elysium
{
    /// <summary>
    /// Interaction logic for Admin.xaml
    /// </summary>
    public partial class Admin : Page
    {

        public Admin()
        {
            InitializeComponent();
        }


        private void btnAddMember_Click(object sender, RoutedEventArgs e)
        {
            AddMember addMemberPage = new AddMember();
            AdminPanel.NavigationService.Navigate(addMemberPage);
        }

        private void btnViewMembers_Click(object sender, RoutedEventArgs e)
        {
            ViewMembers viewMembersPage = new ViewMembers();
            AdminPanel.NavigationService.Navigate(viewMembersPage);
        }

        private void btnMainMenu_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new MainMenu());
        }
    }
}
