﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using Microsoft.Win32;
using System.Globalization;
using System.Threading;
using System.Text.RegularExpressions;
using System.IO;
using Elysium.model;

namespace Elysium
{
    /// <summary>
    /// Interaction logic for AddMember.xaml
    /// </summary>
    public partial class AddMember : Page
    {
        private string picDir = $@"{AppDomain.CurrentDomain.BaseDirectory}pics\";

        private string profilePicURI;
        public AddMember()
        {
            InitializeComponent();
            NavigationCommands.BrowseBack.InputGestures.Clear();
            NavigationCommands.BrowseForward.InputGestures.Clear();
            joiningDate.SelectedDate = DateTime.Today;
            profilePicURI = Path.Combine(picDir, "default-profile-pic.jpg");
            CultureInfo ci = CultureInfo.CreateSpecificCulture(CultureInfo.CurrentCulture.Name);
            ci.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy";
            Thread.CurrentThread.CurrentCulture = ci;
        }


        private void btnAddImage_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog op = new OpenFileDialog();
            op.Title = "Select a picture";
            op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" + "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" + "Portable Network Graphic (*.png)|*.png";
            if (op.ShowDialog() == true)
            {
                BitmapImage newImg = new BitmapImage(new Uri(op.FileName));
                newImg.DecodePixelHeight = 25;
                newImg.DecodePixelWidth = 25;
                userImage.Source = newImg;
                profilePicURI = op.FileName;
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (tbMemberID.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Please Enter Member ID");
                return;
            }
            if (tbName.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Please Enter Member Name");
                return;
            }

            Member newMember = new Member();
            if (newMember.IsIDUnique(Int32.Parse(tbMemberID.Text)))
            {
                MessageBox.Show("Member ID already exists");
                return;
            }
            if (cbGender.Text == "Female")
                newMember.MemberGender = Gender.Female;
            if (cbMembershipType.Text == "Premium")
                newMember.MembershipType = MemberType.Premium;
            else if (cbMembershipType.Text == "Cardio")
                newMember.MembershipType = MemberType.Cardio;
            try
            {
                string newProfilePicPath = Path.Combine(picDir, $"{tbMemberID.Text}.jpg");  //// Create path for new profile pic
                File.Copy(profilePicURI, newProfilePicPath, true);  //// Copy selected pic to the project directory
                newMember.EnterMemberDetails(UInt32.Parse(tbMemberID.Text), tbName.Text, tbFatherName.Text, tbAddress.Text, tbPhoneNo.Text, tbEmail.Text, joiningDate.SelectedDate.GetValueOrDefault(), joiningDate.SelectedDate.GetValueOrDefault(), newProfilePicPath);
                newMember.SaveMemberToDatabase();
                MessageBox.Show("New Member Added");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to write user to database: \n" + ex.Message);
            }

        }



        /// <summary>
        /// Member ID Textbox check. Only numberic text. Max length 6
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MemberIdValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text) || (tbMemberID.Text.Length >= 6);
        }

        /// <summary>
        /// Phone Number Textbox check. Only numberic text. Max length 11
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PhoneNoValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text) || (tbPhoneNo.Text.Length >= 11);
        }

        private void NameValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^a-zA-Z]+");
            e.Handled = regex.IsMatch(e.Text) || (tbName.Text.Length >= 32);
        }

        private void FatherNameValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^a-zA-Z]+");
            e.Handled = regex.IsMatch(e.Text) || (tbFatherName.Text.Length >= 32);
        }
    }
}
