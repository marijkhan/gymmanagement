﻿using Elysium.model;
using System;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Globalization;

namespace Elysium
{
    /// <summary>
    /// Interaction logic for ViewMembers.xaml
    /// </summary>
    /// 
    public class EnumConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (Enum.TryParse(value.ToString(), true, out Gender myGender))
                return myGender;
            if (Enum.TryParse(value.ToString(), true, out MemberType myType))
                return myType;
            else
                return 0;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value.ToString();
        }
    }
    public partial class ViewMembers : Page
    {

        DataTable memberTable = new DataTable();
        MemberDatabase members = MemberDatabase.GetApplicationDatabase();

        public ViewMembers()
        {
            InitializeComponent();
            try
            {
                memberTable = members.GetAllMembers();
                memberTable.AsEnumerable().ToList().ForEach(row =>
                {
                    var cellList = row.ItemArray.ToList();
                    row.ItemArray = cellList.Select(x => x.ToString().Trim()).ToArray();
                });
                this.DataContext = memberTable;
                memberDataGrid.ItemsSource = memberTable.DefaultView;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DataRow selectedRow = ((System.Data.DataRowView)(memberDataGrid.SelectedValue)).Row;
                members.DeleteMember(UInt32.Parse(selectedRow["MemberID"].ToString()));
                memberTable = members.GetAllMembers();
                memberDataGrid.ItemsSource = memberTable.DefaultView;
                MessageBox.Show("Deleted");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }


        

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DataRow selectedRow = ((System.Data.DataRowView)(memberDataGrid.SelectedValue)).Row;
                Member memberToEdit = new Member();
                switch (selectedRow["Gender"].ToString().ToLower().Trim())
                {
                    case "male":
                        memberToEdit.MemberGender = Gender.Male;
                        break;
                    case "female":
                        memberToEdit.MemberGender = Gender.Female;
                        break;
                    default:
                        MessageBox.Show("Please Select A Valid Gender");
                        return;
                }
                switch (selectedRow["MembershipType"].ToString().ToLower().Trim())
                {
                    case "basic":
                        memberToEdit.MembershipType = MemberType.Basic;
                        break;
                    case "cardio":
                        memberToEdit.MembershipType = MemberType.Cardio;
                        break;
                    case "premium":
                        memberToEdit.MembershipType = MemberType.Premium;
                        break;
                    default:
                        MessageBox.Show("Please Select A Valid Membership Type");
                        return;
                }
                switch (selectedRow["Status"].ToString().ToLower().Trim())
                {
                    case "active":
                        memberToEdit.MembershipStatus = Status.Active;
                        break;
                    case "inactive":
                        memberToEdit.MembershipStatus = Status.Inactive;
                        break;
                    default:
                        MessageBox.Show("Please Select A Valid Status");
                        return;
                }

                memberToEdit.MemberID = UInt32.Parse(selectedRow["MemberID"].ToString());
                memberToEdit.Name = selectedRow["Name"].ToString();
                memberToEdit.FatherName = selectedRow["FatherName"].ToString();
                memberToEdit.Address = selectedRow["Address"].ToString();
                memberToEdit.PhoneNo = selectedRow["Phone"].ToString();
                memberToEdit.Email = selectedRow["Email"].ToString();
                memberToEdit.JoiningDate = (DateTime)selectedRow["JoiningDate"];
                members.UpdateMember(memberToEdit);
                MessageBox.Show("Updated");

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void btnChangeStatus_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DataRow selectedRow = ((System.Data.DataRowView)(memberDataGrid.SelectedValue)).Row;
                DateTime joiningDate = (DateTime)selectedRow["JoiningDate"];
                DateTime paymentDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, joiningDate.Day);
                uint memberID = (uint)(int)selectedRow["MemberID"];
                members.UpdatePaymentDate(memberID, paymentDate);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
