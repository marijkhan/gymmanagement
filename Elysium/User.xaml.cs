﻿using Elysium.model;
using System;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Elysium
{
    /// <summary>
    /// Interaction logic for User.xaml
    /// </summary>
    public partial class User : Page
    {
        public User()
        {
            InitializeComponent();
            

        }

        private void btnAdmin_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Admin());
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(tbSearch.Text))
                return;
            MemberDatabase memData = MemberDatabase.GetApplicationDatabase();
            Regex regex = new Regex("[0-9]+");
            Member myMember = new Member();

            try
            {
                if (regex.IsMatch(tbSearch.Text))
                {
                    if (tbSearch.Text.Length < 7)
                        myMember = memData.SearchByID((uint)Int32.Parse(tbSearch.Text));
                    else
                    {
                        MessageBox.Show("phone");
                        myMember = memData.SearchByPhone(tbSearch.Text);

                    }
                }
                else
                    myMember = memData.SearchByName(tbSearch.Text);
                if (myMember.Name == null)
                    MessageBox.Show("Member not found");
                else
                {
                    tbMemberID.Text = myMember.MemberID.ToString();
                    tbName.Text = myMember.Name;
                    tbFatherName.Text = myMember.FatherName;
                    tbGender.Text = myMember.MemberGender.ToString();
                    tbMemberType.Text = myMember.MembershipType.ToString();
                    if (myMember.MembershipStatus == Status.Active)
                    {
                        lblStatus.Content = "Paid";
                        lblStatus.Foreground = Brushes.Green;
                    }
                    else
                    {
                        lblStatus.Content = "Unpaid";
                        lblStatus.Foreground = Brushes.Red;
                    }
                    BitmapImage newImg = new BitmapImage(new Uri(myMember.ProfilePicture));
                    joiningDate.Content = myMember.JoiningDate.ToString("dd MMM yyyy");
                    userImage.Source = newImg;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            }

        private void btnMainMenu_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new MainMenu());
        }

        private void tbSearch_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            e.Handled = (tbSearch.Text.Length >= 25);
        }

        private void tbSearch_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                btnSearch_Click(this, null);
            }
        }

        private void btnMarkAttendance_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Attendance Marked");
            this.NavigationService.Navigate(new User());
            
        }
    }
}
