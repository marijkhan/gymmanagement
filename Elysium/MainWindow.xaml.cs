﻿using Elysium.model;
using System.Windows;

namespace Elysium
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Main.NavigationService.Navigate(new MainMenu());
            MemberDatabase memberDatabase = MemberDatabase.GetApplicationDatabase();
            memberDatabase.UpdateStatus();
        }

        

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }

        private void btnMinimize_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

    }
}
