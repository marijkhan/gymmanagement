﻿using System;
using System.Collections.Generic;

namespace Elysium.model
{
    public enum Gender { Male, Female }
    public enum Status { Inactive, Active }
    public enum MemberType { Basic, Cardio, Premium }

    class Member
    {
       

        public uint MemberID { get; set; }
        public string Name { get; set; }
        public string FatherName { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string PhoneNo { get; set; }
        public Gender MemberGender { get; set; }
        public Status MembershipStatus { get; set; }
        public MemberType MembershipType { get; set; }
        public DateTime JoiningDate { get;  set; }
        public DateTime PaymentDate { get; set; }
        public string ProfilePicture { get; set; }

        public Member()
        {
            this.MembershipType = MemberType.Basic;
            this.MemberGender = Gender.Male;
            this.MembershipStatus = Status.Inactive;
        }

        public Member(uint memberID, string name, string fatherName, string address, string phoneNo, string email, DateTime joiningDate, DateTime paymentDate, Status membershipStatus, string profilePicture)
        {
            this.MemberID = memberID;
            this.Name = name;
            this.FatherName = fatherName;
            this.Address = address;
            this.PhoneNo = phoneNo;
            this.Email = email;
            this.JoiningDate = joiningDate;
            this.PaymentDate = paymentDate;
            this.MembershipType = MemberType.Basic;
            this.MemberGender = Gender.Male;
            this.MembershipStatus = membershipStatus;
            this.ProfilePicture = profilePicture ?? throw new ArgumentNullException(nameof(profilePicture));
        }

        public void EnterMemberDetails(uint memberID, string name, string fatherName, string address, string phoneNo, string email, DateTime joiningDate, DateTime paymentDate, string profilePicture)
        {
            this.MemberID = memberID;
            this.Name = name;
            this.FatherName = fatherName;
            this.Address = address;
            this.PhoneNo = phoneNo;
            this.Email = email;
            this.JoiningDate = joiningDate;
            this.PaymentDate = paymentDate;
            this.ProfilePicture = profilePicture ?? throw new ArgumentNullException(nameof(profilePicture));
        }

        public void SaveMemberToDatabase()
        {
            MemberDatabase myDatabase = MemberDatabase.GetApplicationDatabase();
            myDatabase.AddToMembers(this);
        }

        public bool IsIDUnique(int userID)
        {
            List<int> ids = new List<int>();
            MemberDatabase myDatabase = MemberDatabase.GetApplicationDatabase();
            ids = myDatabase.GetMemberIDs();
            if (ids.Contains(userID))
                return true;
            return false;
        }

        
        
    }
}
