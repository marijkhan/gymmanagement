﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace Elysium.model
{
    class MemberDatabase
    {
        private string databaseConnectionString = ConfigurationManager.ConnectionStrings["connString"].ConnectionString;

        private MemberDatabase() { }
        private static MemberDatabase instance = null;

        public static MemberDatabase GetApplicationDatabase()
        {
            if (instance == null)
                instance = new MemberDatabase();
            return instance;
        }

        public DataTable GetAllMembers()
        {
            DataTable table = new DataTable("Member");
            string query = "SELECT * FROM MEMBER";

            using (SqlConnection cn = new SqlConnection(databaseConnectionString))
            using (SqlCommand cmd = new SqlCommand(query, cn))
            {
                SqlDataAdapter data = new SqlDataAdapter(cmd);
                try
                {
                    cn.Open();
                    data.Fill(table);
                    cn.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine("Unable to fetch data", e.Message);
                }
                           
            }
            return table;
        }

        public void DeleteMember(uint MemberID)
        {
            string query = "DELETE FROM Member WHERE MemberID = @MemberID";
            using (SqlConnection cn = new SqlConnection(databaseConnectionString))
            using (SqlCommand cmd = new SqlCommand(query, cn))
            {
                cmd.Parameters.Add("@MemberID", SqlDbType.Int).Value = MemberID;
                try
                {
                    cn.Open();
                    cmd.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                finally
                {
                    cn.Close();
                }
            }
        }      

        public void AddToMembers(Member newMember)
        {      
            string query = "INSERT INTO Member " +
                   "VALUES (@MemberID, @Name, @FatherName, @Gender, @Phone, @Email, @MembershipType, @Address, @JoiningDate, @Status, @PaymentDate, @ProfilePicPath) ";
            using (SqlConnection cn = new SqlConnection(databaseConnectionString))
            using (SqlCommand cmd = new SqlCommand(query, cn))
            {
                // define parameters and their values
                cmd.Parameters.Add("@MemberID", SqlDbType.Int).Value = newMember.MemberID;
                cmd.Parameters.Add("@Name", SqlDbType.NVarChar, 32).Value = newMember.Name;
                cmd.Parameters.Add("@FatherName", SqlDbType.NVarChar, 32).Value = newMember.FatherName;
                cmd.Parameters.Add("@Gender", SqlDbType.NChar, 10).Value = newMember.MemberGender;
                cmd.Parameters.Add("@Phone", SqlDbType.NChar, 12).Value = newMember.PhoneNo;
                cmd.Parameters.Add("@Email", SqlDbType.NVarChar, 32).Value = newMember.Email;
                cmd.Parameters.Add("@MembershipType", SqlDbType.NChar, 10).Value = newMember.MembershipType;
                cmd.Parameters.Add("@Address", SqlDbType.NVarChar, 60).Value = newMember.Address;
                cmd.Parameters.Add("@JoiningDate", SqlDbType.Date).Value = newMember.JoiningDate;
                cmd.Parameters.Add("@Status", SqlDbType.NChar, 8).Value = newMember.MembershipStatus;
                cmd.Parameters.Add("@PaymentDate", SqlDbType.Date).Value = newMember.PaymentDate;
                cmd.Parameters.Add("@ProfilePicPath", SqlDbType.NVarChar).Value = newMember.ProfilePicture;

                //// open connection, execute INSERT, close connection
                try
                {
                    cn.Open();
                    cmd.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                finally
                {
                    cn.Close();
                }
            }

        }

        public void UpdateMember(Member newMember)
        {
            string query = "UPDATE MEMBER SET Name = @Name, FatherName = @FatherName, Gender = @Gender, Phone = @Phone, Email = @Email, MembershipType = @MembershipType, Address = @Address, JoiningDate = @JoiningDate, Status = @Status WHERE MemberID = @MemberID";
            using (SqlConnection cn = new SqlConnection(databaseConnectionString))
            using (SqlCommand cmd = new SqlCommand(query, cn))
            {
                try
                {
                    cmd.Parameters.Add("@MemberID", SqlDbType.Int).Value = newMember.MemberID;
                    cmd.Parameters.Add("@Name", SqlDbType.NVarChar, 32).Value = newMember.Name;
                    cmd.Parameters.Add("@FatherName", SqlDbType.NVarChar, 32).Value = newMember.FatherName;
                    cmd.Parameters.Add("@Gender", SqlDbType.NChar, 10).Value = newMember.MemberGender;
                    cmd.Parameters.Add("@Phone", SqlDbType.NChar, 12).Value = newMember.PhoneNo;
                    cmd.Parameters.Add("@Email", SqlDbType.NVarChar, 32).Value = newMember.Email;
                    cmd.Parameters.Add("@MembershipType", SqlDbType.NChar, 10).Value = newMember.MembershipType;
                    cmd.Parameters.Add("@Address", SqlDbType.NVarChar, 60).Value = newMember.Address;
                    cmd.Parameters.Add("@JoiningDate", SqlDbType.Date).Value = newMember.JoiningDate;
                    cmd.Parameters.Add("@Status", SqlDbType.NChar, 8).Value = newMember.MembershipStatus;
                    cn.Open();
                    cmd.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                finally
                {
                    cn.Close();
                }
            }

        }

        public void UpdateStatus()
        {
            string query = @"UPDATE member set Status = 'Inactive' where (DATEADD(month, 1, PaymentDate) < Convert(Date, GetDate(), 101) and Status = 'Active')";
            using (SqlConnection cn = new SqlConnection(databaseConnectionString))
            using (SqlCommand cmd = new SqlCommand(query, cn))
            {               
                try
                {
                    cn.Open();
                    cmd.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                finally
                {
                    cn.Close();
                }
            }
        }

        public void UpdatePaymentDate(uint memberID, DateTime paymentDate)
        {
            string query = @"UPDATE member set Status = 'Active', PaymentDate = @PaymentDate where MemberID = @MemberID";
            using (SqlConnection cn = new SqlConnection(databaseConnectionString))
            using (SqlCommand cmd = new SqlCommand(query, cn))
            {
                try
                {
                    cmd.Parameters.Add("@MemberID", SqlDbType.Int).Value = memberID;
                    cmd.Parameters.Add("@PaymentDate", SqlDbType.Date).Value = paymentDate;

                    cn.Open();
                    cmd.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                finally
                {
                    cn.Close();
                }
            }
        }

        public Member SearchByID(uint memberID)
        {
            Member memberResult = new Member();
            string query = @"SELECT * from member where MemberID = @MemberID";
            using (SqlConnection cn = new SqlConnection(databaseConnectionString))
            using (SqlCommand cmd = new SqlCommand(query, cn))
            {
                try
                {
                    cmd.Parameters.Add("@MemberID", SqlDbType.Int).Value = memberID;
                    cn.Open();

                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        reader.Read();
                        memberResult.MemberID = (uint)(int)reader["MemberID"];
                        memberResult.Name = reader["Name"].ToString();
                        memberResult.FatherName = reader["FatherName"].ToString();
                        Enum.TryParse(reader["Gender"].ToString(), true, out Gender gender);
                        memberResult.MemberGender = gender;
                        Enum.TryParse(reader["MembershipType"].ToString(), true, out MemberType memType);
                        memberResult.MembershipType = memType;
                        Enum.TryParse(reader["Status"].ToString(), true, out Status myStatus);
                        memberResult.MembershipStatus = myStatus;
                        memberResult.ProfilePicture = reader["ProfilePicPath"].ToString();
                        memberResult.JoiningDate = (DateTime)reader["JoiningDate"];


                        Console.WriteLine(memberResult.MemberID);
                    }
                    reader.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                finally
                {
                    cn.Close();
                }
            }
            return memberResult;
        }

        public Member SearchByPhone(string phone)
        {
            Member memberResult = new Member();
            string query = "SELECT * from member where Phone Like @SEARCH";

            using (SqlConnection cn = new SqlConnection(databaseConnectionString))
            using (SqlCommand cmd = new SqlCommand(query, cn))
            {
                try
                {
                    cmd.Parameters.AddWithValue("@SEARCH", "%" + phone + "%");
                    Console.WriteLine(cmd.CommandText);
                    cn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        reader.Read();
                        memberResult.MemberID = (uint)(int)reader["MemberID"];
                        memberResult.Name = reader["Name"].ToString();
                        memberResult.FatherName = reader["FatherName"].ToString();
                        Enum.TryParse(reader["Gender"].ToString(), true, out Gender gender);
                        memberResult.MemberGender = gender;
                        Enum.TryParse(reader["MembershipType"].ToString(), true, out MemberType memType);
                        memberResult.MembershipType = memType;
                        Enum.TryParse(reader["Status"].ToString(), true, out Status myStatus);
                        memberResult.MembershipStatus = myStatus;
                        memberResult.ProfilePicture = reader["ProfilePicPath"].ToString();
                        memberResult.JoiningDate = (DateTime)reader["JoiningDate"];
                        Console.WriteLine(memberResult.MemberID);
                    }
                    reader.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                finally
                {
                    cn.Close();
                }
            }
            return memberResult;
        }

        public Member SearchByName(string name)
        {
            Member memberResult = new Member();
            string query = "SELECT * from member where Name Like @SEARCH";

            using (SqlConnection cn = new SqlConnection(databaseConnectionString))
            using (SqlCommand cmd = new SqlCommand(query, cn))
            {
                try
                {
                    cmd.Parameters.AddWithValue("@SEARCH", "%" + name + "%");
                    Console.WriteLine(cmd.CommandText);
                    cn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        reader.Read();
                        memberResult.MemberID = (uint)(int)reader["MemberID"];
                        memberResult.Name = reader["Name"].ToString();
                        memberResult.FatherName = reader["FatherName"].ToString();
                        Enum.TryParse(reader["Gender"].ToString(), true, out Gender gender);
                        memberResult.MemberGender = gender;
                        Enum.TryParse(reader["MembershipType"].ToString(), true, out MemberType memType);
                        memberResult.MembershipType = memType;
                        Enum.TryParse(reader["Status"].ToString(), true, out Status myStatus);
                        memberResult.MembershipStatus = myStatus;
                        memberResult.ProfilePicture = reader["ProfilePicPath"].ToString();
                        memberResult.JoiningDate = (DateTime)reader["JoiningDate"];
                        Console.WriteLine(memberResult.MemberID);
                    }
                    reader.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                finally
                {
                    cn.Close();
                }
            }
            return memberResult;
        }



        //public DataTable GetPaymentDates()
        //{
        //    DataTable table = new DataTable("Payment Dates");
        //    string query = "SELECT MemberID,PaymentDate FROM MEMBER";

        //    using (SqlConnection cn = new SqlConnection(databaseConnectionString))
        //    using (SqlCommand cmd = new SqlCommand(query, cn))
        //    {
        //        SqlDataAdapter data = new SqlDataAdapter(cmd);
        //        try
        //        {
        //            cn.Open();
        //            data.Fill(table);
        //            cn.Close();
        //        }
        //        catch (Exception e)
        //        {
        //            Console.WriteLine("Unable to fetch data", e.Message);
        //        }

        //    }
        //    return table;
        //}

        public List<int> GetMemberIDs()
        {
            List<int> memberIDs = new List<int>();
            string query = "select MemberID from Member";
            using (SqlConnection cn = new SqlConnection(databaseConnectionString))
            {
                cn.Open();
                using (SqlCommand cmd = new SqlCommand(query, cn))
                {
                    cmd.CommandType = CommandType.Text;
                    SqlDataReader reader = cmd.ExecuteReader();
                    while(reader.Read())
                    {
                        memberIDs.Add(Int32.Parse(reader["MemberID"].ToString()));
                    }
                }
            }
            return memberIDs;
        }
    }
}
